const express = require('express');
const nanoid = require('nanoid');
const Link = require('../models/Link');

const router = express.Router();

const createRouter = db => {

    router.get('/', (req, res) => {
        Link.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', (req, res) => {
        const linkData = req.body;

        linkData.shortURL = nanoid();

        Link.findOne({shortURL: linkData.shortURL}).then(result => {
            if (result) {
                linkData.shortURL = nanoid(10);

                const link = new Link(linkData);

                link.save()
                    .then(() => res.send(link))
                    .catch(error => res.status(400).send(error));
            } else {
                const link = new Link(linkData);

                link.save()
                    .then(() => res.send(link))
                    .catch(error => res.status(400).send(error));
            }
        });
    });

    router.get('/:shortURL', (req, res) => {
        Link.findOne({shortURL: req.params.shortURL})
            .then(result => {
                if (result) res.status(301).redirect(result.originalUrl);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;